export const TOTAL_AFFECTED_LABEL_ID = 0;
export const TOTAL_AFFECTED_ID = 1; 
export const TOTAL_DEATH_LABEL_ID = 6;
export const TOTAL_DEATH_ID = 7;
export const TOTAL_RECOVERED_LABEL_ID = 8;
export const TOTAL_RECOVERED_ID = 9;

export const ALL_TOTAL_IDS = [
  {
    labelId: TOTAL_AFFECTED_LABEL_ID,
    dataId:  TOTAL_AFFECTED_ID,
  },
  {
    labelId: TOTAL_DEATH_LABEL_ID,
    dataId:  TOTAL_DEATH_ID,
  },
  {
    labelId: TOTAL_RECOVERED_LABEL_ID,
    dataId:  TOTAL_RECOVERED_ID,
  }
];
