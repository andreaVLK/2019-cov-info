export const TITLE = 'nCoV-2019 info';
export const DESCRPTION = 'Global affected, deaths and recovered';
export const STATES_INFO_TITLE = 'Affected by states';
export const DISCLAIMER = 'Data source: John Hopkins CSSE - https://gisanddata.maps.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6';
