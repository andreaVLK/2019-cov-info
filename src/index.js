import puppeteer from "puppeteer";
import * as CONSTANTS from "./constants";
import { getGeneralInfo, getStatesInfo, printAllInfo, print } from "./utils"

export const start = async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  try {
    await page.goto(CONSTANTS.SITE_URL);
    await page.waitFor(".responsive-text-group");

    const generalInfo = await getGeneralInfo({ page });
    const statesInfo = await getStatesInfo({ page });
  
    printAllInfo({ generalInfo, statesInfo })
    await browser.close();
  } catch {
    print('Service temporary unavailable. Retry')
    await browser.close();
    
  }
};
