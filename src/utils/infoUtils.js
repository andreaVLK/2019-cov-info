import * as CONSTANTS from '../constants'

export const getGeneralInfo = async ({ page }) =>
  page.evaluate(async (CONSTANTS) => {
    const genralInfo = [];
    CONSTANTS.ALL_TOTAL_IDS.forEach(async idObj => {
      const label = await document.getElementsByTagName("text")[idObj.labelId].innerHTML;
      const data = await document.getElementsByTagName("text")[idObj.dataId].innerHTML;
      genralInfo.push({
        data: data.replace(',','.'),
        label,
      })
    })
    return genralInfo
  }, CONSTANTS);

export const getStatesInfo = async ({ page }) =>
  page.evaluate(async () => {
    const statesInfo = [];
    for (let i = 0; i < 20; i++) {
      const state = await document
        .getElementsByClassName("list-item-content")
        [i].getElementsByTagName("span")[2].innerHTML;
      const affected = await document
        .getElementsByClassName("list-item-content")
        [i].getElementsByTagName("span")[0]
        .getElementsByTagName("strong")[0].innerHTML;
      statesInfo.push({
        state,
        affected: affected.replace(',','.')
      });
    }
    return statesInfo;
  });