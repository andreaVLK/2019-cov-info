
import * as CONSTANTS from '../constants'

export const print = (text) => console.log("\x1b[34m",text || '',"\x1b[37m");
export const printInfo = (label, info) => console.log("\x1b[37m",`${label}:`,"\x1b[32m",`${info}`,"\x1b[37m");

export const printGeneralInfo = (generalInfo) => {
  generalInfo.forEach(({ label, data }) => {
    printInfo(label, data);
  });
}

export const printStatesInfo = (statesInfo) => {
  statesInfo.forEach(({ state, affected }) => {
    printInfo(state, affected);
  });
}

export const printAllInfo = ({ generalInfo, statesInfo}) => {
  print(CONSTANTS.TITLE);
  print(CONSTANTS.DISCLAIMER);
  print();
  printGeneralInfo(generalInfo);
  print();
  print();
  print(CONSTANTS.STATES_INFO_TITLE);
  print();
  printStatesInfo(statesInfo);
  print();
}