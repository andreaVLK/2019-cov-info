## 2019-cov-info

Real time information about nCoV-2019 affections.

### To get real time data:

`npm start`

Data source: John Hopkins CSSE 
https://gisanddata.maps.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6